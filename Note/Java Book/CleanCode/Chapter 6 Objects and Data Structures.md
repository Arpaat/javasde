# Chapter 6 Objects and Data Structures

Objects: 暴露行为-接口

数据结构：没有明显的行为接口，例如 DTO，entity

### Data Abstraction

Hide your variables in your objects - this is called abstraction

A class does not simply push its variables out through getters and setters. Rather it exposes abstract interfaces that allow its users to manipulate the *essence* of the data, without having to know its implementation.

Do not expose the detail of the data, rather we need to express our data in abstract terms.

```java
public class A {
    public string name;
    
    public string getName() { return name; }
}

public class B {
    private string age;  // hide data
    
    public string yourAge() { return age; }  // abstraction method to get data
}
```



### Data/Object Anti-Symmetry

**Objects hide their data behind abstractions and expose functions that operate on that data;**

**Data Structure expose their data and have no meaningful functions;**

These can be concluded as :

Procedural code makes it hard to add new data structures because all the functions must change;

Object-oriented code makes it hard to add new functions becasue all the class must change;

what should we do ?

If you want to add new data types rather than new functions, OO is good;

If you want to add new functions , procedure code is good;

```java
// procedure code
public class square{
    int a;
    int b;
}

public class circle{
    int r;
}

public int area(Object x) {
    if(x instanceof square) {
        return a*b;
    } else if (x instanceof circle) {
        return 2*r^2;
    }
}

// oo code
public class square{
    int a;
    int b;
    public int getArea() {
        return a*b;
    }
}

public class circle{
    int r;
    public int getArea(){
        return 2*r^2;
    }
}
```



### The Law of Demeter

a module should not know about the innards of the objects is manipulates.

More precisely, a method f of a class C should only call the methods of these:

* C
* An object created by f
* An object passed as an argument to f
* An object held in an instance variable of C

Talk to friends, not strangers

Don`t write code like chain



### Data Transfer Object

DTOs are useful structure when communicating with databases or parsing messages from sockets and so on. They often become the first in a series of translations stages that convert raw data in a database into objects in the application code.
