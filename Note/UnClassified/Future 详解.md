# Future 详解

Java中的Future表示一个**异步计算任务**，当任务完成时可以得到计算结果，如果我们希望一旦计算完成就拿到结果展示给用户或者做计算，就必须使用另一个线程不断的查询计算状态，这样会使代码复杂而且效率低下。使用guava的**ListenableFuture**可以帮我们检测Future是否完成了，如果完成就会自动调用回调函数，这样可以减少并发程序的复杂度。

**ListenableFuture是可以监听的Future**，它是对Java原生的Future进行了拓展和增强。在java中Future表示一个多线程异步执行的任务，当任务执行完成之后可以得到一个计算结果。如果我们希望一旦计算完成之后就可以拿到结果返回或者将结果做另外的计算操作，就必须使用线程去不断查询计算状态。这样做会导致代码复杂，并且计算效率低下。**使用ListenableFuture Guava帮我们检测Future是否完成了，如果完成就自动调用回调函数，这样可以减少并发程序的复杂度。**

有了ListenableFuture实例，有两种方法可以执行此Future并执行Future完成之后的回调函数。推荐使用第二种方法，因为第二种方法可以直接得到Future的返回值，或者处理错误情况。本质上第二种方法是通过调动第一种方法实现的，做了进一步的封装。

举个工程中的例子：

```java
// 实例化一个ListenableFuture对象
ListenableFuture<RecordMetadata> future = KafkaProducers.sendProto(topic, notifyMessage);
//addCallback 调用回掉函数
Futures.addCallback(future, new FutureCallback<RecordMetadata>() {
      public void onSuccess(RecordMetadata result) { 
          logger.info("send audit result success,offset:{}, data:{}", result.offset(), toJSON(notifyMessage));
      }
      public void onFailure(Throwable t) {
          logger.error("send audit result fail, data:{}", toJSON(notifyMessage), t);
      }
}, EXECUTOR);
          
```

## 添加监听 addListener

```java
listenableFuture.addListener () -> {
    sout("listen success");
    doSomething();
},executorService;
```

## 添加回掉 addCallback

```java
Futures.addCallback(listenableFuture, new FutureCallback<Object>) {
    @Override
    public void onSuccess(@Nullable Object result) {
        sout(result);
    }
    @Override
    public void onFailure(Throwable t) {
 
    }
},executorService;
```





### 参考文档

https://blog.csdn.net/inrgihc/article/details/119109804