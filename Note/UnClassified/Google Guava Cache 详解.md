# Google Guava Cache 详解

## 基础信息

Google Guava Cache是一种非常优秀本地缓存解决方案，提供了基于容量，时间和引用的缓存回收方式。基于容量的方式内部实现采用LRU算法，基于引用回收很好的利用了Java虚拟机的垃圾回收机制。其中的缓存构造器CacheBuilder采用构建者模式提供了设置好各种参数的缓存对象，缓存核心类LocalCache里面的内部类Segment与jdk1.7及以前的ConcurrentHashMap非常相似，都继承于ReetrantLock，还有六个队列，以实现丰富的本地缓存方案。

> 为什么要使用本地缓存？

**相对于IO操作**，速度快，效率高
**相对于Redis**，Redis是一种优秀的分布式缓存实现，受限于网卡等原因，远水救不了近火。

**DB + Redis + LocalCache = 高效存储，高效访问**

> 本地缓存就是将数据存储在jvm中方便及时存取

## 如何使用Cache

设置缓存容量、超时时间、提供移除监听器、缓存加载器、构建缓存

## LoadingCache

LoadingCache是Guava库中最常用的核心功能之一，其定位是作为一个支持多线程并发读写、高性能、通用的in-heap本地缓存。在使用上LoadingCache通过builder模式提供描述式api定制LoadingCache的功能以满足给定场景的需求。核心功能有：

- 核心功能：高性能线程安全的in-heap Map数据结构 （类似于ConcurrentHashMap）
- 支持key不存在时按照给定的CacheLoader的loader方法进行Loading，如果有多个线程同时get一个不存在的key，那么有一个线程负责load，其他线程wait结果
- 支持entry的evitBySize，这是一个LRU cache的基本功能
- 支持对entry设置过期时间，按照最后访问/写入的时间来淘汰entry
- 支持传入entry删除事件监听器，当entry被删除或者淘汰时执行监听器逻辑
- 支持对entry进行定期reload，默认使用loader逻辑进行同步reload (建议override CacheLoader的reload方法实现异步reload)
- 支持用WeakReference封装key，当key在应用程序里没有别的引用时，jvm会gc回收key对象，LoadingCache也会得到通知从而进行清理
- 支持用WeakReference或者SoftReference封装value对象
- 支持缓存相关运行数据指标的统计
- 集成了多部操作，调用get方法时可以在未命中缓存时从其他地方获取数据源（Redis，DB）等，并加载到缓存中。

### 基础数据结构

LoadingCache 是一个支持多线程并发操作的key-value结构的Map，类似于concurrentHashMap。将原来的一个hash表分成多段，所有读写操作都会先寻址到对应的段，在段内进行，这样通过分段锁来避免写操作时锁住整个表(这里借鉴的是ConcurrentHashMap在java8之前的实现，自java8开始ConcurrentHashMap为了进一步提高并发，摒弃了分段锁实现，基于CAS机制直接采用一个大数组，同时为了提高哈希碰撞下的寻址性能，Java8在链表长度超过一定阈值时将链表转换为红黑树)。

在每个段内有个独立的数组作为hash表，采用**链表来解决hash碰撞**，当hash表容量达到一定阈值时会进行扩容，然后rehash，都是常规操作。

值得注意的是：**cache每个段内的写操作是需要加锁的**，但是读操作基本都是不加锁的。也就是说对于写操作，同时只允许一个写操作的存在，但是对于读操作是没有限制的，读操作也完全可以和写操作并发进行。

### 淘汰机制

#### 基于容量的淘汰机制

初始化时根据总的maxSize计算得到每个段的maxSize，保证每个段内元素大小不超过段内的元素数量限制。通过CacheBuilder.maximumSize设置cache的最大容量数，当超过此最大容量时，cache会根据LRU算法清除缓存。

#### 基于过期时间的淘汰机制

将清理元素操作嵌入到读写过程中，在每次写操作时都会去尝试执行清理逻辑，在累积一定读操作后也会去尝试执行清理逻辑，以保证在一个少写多读的场景下过期元素也能有机会去清理。

#### 基于java引用的淘汰机制

由于LoadingCache的缓存属性，其内部Map中的Key和Value都可以用Java中Reference类来包装。**对于Key支持用WeakReference包装，当Key对象除了Reference之外没有别的地方引用时，下次gc时对象会被回收**，同时ReferenceQueue里会收到通知，然后对应的entry会被清理掉。**对于Value支持用WeakReference/SoftReference包装，当Value对象没有别处引用时，分别会在下次gc/即将OOM前被回收掉**，同样对应的ReferenceQueue会收到通知，对应的entry会被清理。 当使用Reference来包装Key/Value时，相等性校验则直接比较的是对象引用地址是否相同，hash计算也是采用native的System.identityHashCode方法。被GC回收元素的清理时机和过期元素的清理时机/过程基本一致。



## 代码解析

创建一个processConfigCache，类型为<String, Optional<Process>>的Map，key是processConfig，value是process。

```java
private final LoadingCache<String, Optional<Process>> processConfigCache = KsCacheBuilder.newBuilder()
            .maximumSize(MAX_CACHE_SIZE)	//最大容量
            .refreshAfterWrite(DEFAULT_CACHE_REFRESH_TIME, TimeUnit.MILLISECONDS)  //写后更新时间
            .expireAfterWrite(DEFAULT_CACHE_EXPIRE_TIME, TimeUnit.MILLISECONDS)		//写后过期时间
            .build(new AsyncReloadCacheLoader<String, Optional<Process>>() {
                @Override
                public Optional<Process> load(String key) {
                    String[] split = key.split(CACHE_KEY_SEPARATOR);	//缓存key 按@分开
                    if (split.length < 2) {
                        return Optional.empty();
                    }
                    String processConfig = processConfigService.getProcessConfig(split[0], Integer.parseInt(split[1]));
                    Process process = null;
                    try {
                        StopWatch stopWatch = PerfUtils.getWatcher();
                        process = ProcessUtils.buildAndCheckProcess(processConfig, functionNodeParamsParser);	//根据processconfig和功能节点参数解析构建process
                        //perf 监控 讲process 已经加载到缓存中
                        PROCESS_LOAD.perfWithMicros(
                                stopWatch.getTimeMicros(),
                                process.getCode(),
                                String.valueOf(process.getVersion()),
                                String.valueOf(process.getNodeMap().size()),
                                String.valueOf(process.getFlowMap().size()));
                    } catch (Exception e) {
                        logger.error("buildProcess exception! processConfig: {}", processConfig, e);
                        FATAL_ERROR.perf("LOAD_PROCESS_EXCEPTION", e.getClass().getSimpleName());
                    }
                    return Objects.nonNull(process)
                            ? Optional.of(process)
                            : Optional.empty();
                }
            });

```



## 文档

http://kapsterio.github.io/test/2019/03/04/guava-loading-cache.html