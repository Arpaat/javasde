# **Redis和Memcached有什么区别？**

## **性能方面**

Memcached是全内存的数据缓冲系统，Redis虽然支持数据的持久化，但是全内存才是其高性能的本质。作为基于内存的存储系统来说，机器物理内存的大小就是系统能够容纳的最大数据量。如果需要处理的数据量超过了单台机器的物理内存大小，就需要构建分布式集群来扩展存储能力。

Redis只使用单核，而Memcached可以使用多核，所以平均每一个核上Redis在存储小数据时比Memcached性能更高。而在100k以上的数据中，Memcached性能要高于Redis，虽然Redis最近也在存储大数据的性能上进行优化，但是比起Memcached，还是稍有逊色。

## **集群和分布式**

redis 和memcached都支持集群,redis支持**master-slave**模式,memcache可以使用**一致性hash**做分布式。简单两张图看出区别:

Memcached本身并不支持分布式，因此只能在客户端通过像一致性哈希这样的分布式算法来实现Memcached的分布式存储，关于分布式一致性哈希算法见总结：[**分布式一致性hash算法**](https://www.cnblogs.com/Courage129/p/14334821.html)。当客户端向Memcached集群发送数据之前，首先会通过内置的分布式算法计算出该条数据的目标节点，然后数据会直接发送到该节点上存储。但客户端查询数据时，同样要计算出查询数据所在的节点，然后直接向该节点发送查询请求以获取数据。

 ![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcACPMiBd-JaRYlDNRTAXdb7m:-5990935681012378759fcACPMiBd-JaRYlDNRTAXdb7m:1707375948638)

相较于Memcached只能采用客户端实现分布式存储，Redis更偏向于在服务器端构建分布式存储，但没有采用一致性哈希，关于Redis集群分析见总结：[**Redis集群数据没法拆分时的搭建策略**](https://www.cnblogs.com/Courage129/p/14350129.html)。最新版本的Redis已经支持了分布式存储功能。Redis Cluster是一个实现了分布式且允许单点故障的Redis高级版本，它没有中心节点，具有线性可伸缩的功能。为了保证单点故障下的数据可用性，Redis Cluster引入了**Master节点**和**Slave节点**。在Redis Cluster中，每个Master节点都会有对应的两个用于冗余的Slave节点。这样在整个集群中，任意两个节点的宕机都不会导致数据的不可用。当Master节点退出后，集群会自动选择一个Slave节点成为新的Master节点。

 ![img](/Users/arpatnurmamat/CS/HomeBase/Note/Redis/assets/out.png)

## **数据类型**

Redis支持的数据类型要比memcached丰富得多,Redis不仅仅支持简单的K-V类型的数据，同时还提供String，List,Set,Hash,Sorted Set,pub/sub,Transactions数据结构的存储。Redis内部使用一个redisObject对象来表示所有的key和value。

memcache支持K-V数据类型，这个V没有类型的概念,也就意味着通过memcached取得的数据为JSON对象,并且做一次请求,会返回所有的Value数据,你需要将数据拿到客户端来进行类似的修改再set回去，序列化再反序列化，这大大增加了网络IO的次数和数据体积,这时候IO就是瓶颈,因为并不是所有数据都需要,同时存以及取数据时需要客户端自己对数据进行处理。

意味着在对数据进行复杂操作方面,Redis更有优势,因为Redis对每种类型都有独特的操作命令,对于很多操作提升了效率。这体现了大数据方面计算向数据移动的思维。

## **持久性**

Redis和Memcached都是将数据存放在内存中，都是内存数据库。不过memcached还可用于缓存其他东西，例如图片、视频等等。

memcached把数据全部存在内存之中，断电后会挂掉，数据不能超过内存大小；redis有部份存在硬盘上，这样能保证数据的持久性，支持数据的持久化（RDB、AOF），而Memcached不支持持久化。

同时Redis并不是所有的数据都一直存储在内存中的，当物理内存用完时，Redis可以将一些很久没用到的value交换到磁盘，但memcached超过内存比例会抹掉前面的数据。redis支持数据落地持久化存储([**Redis持久化之RDB和AOF**](https://www.cnblogs.com/Courage129/p/14342102.html)),可以将内存中的数据保持在磁盘中，重启的时候可以再次加载进行使用。memcache不支持数据持久存储 ,也就是掉电就没了。

## **Value大小不同**

memcached的简单限制就是键（key）和item的限制。最大键长为250个字符。可以接受的储存数据不能超过1MB，因为这是典型slab(Linux系统分配内存的一种算法) 的最大值。但是我们可以突破对key长度的限制。

修改memcached源文件。在memcached.h中定义key的长度，其代码为：

```
#define KEY_MAX_LENGTH 250
```

更换为所需要的长度，比如：1024

```
#define KEY_MAX_LENGTH 1024
```

## **数据一致性方式不同**

redis使用的是单线程模型，保证了数据按顺序提交。

memcache需要使用cas保证数据一致性。CAS（Check and Set）是一个确保并发一致性的机制，属于“乐观锁”范畴；原理很简单：拿版本号，操作，对比版本号，如果一致就操作，不一致就放弃任何操作 cpu利用,redis单线程模型只能使用一个cpu，但是可以开启多个redis进程。

## **内存管理机制**

对于像Redis和Memcached这种基于内存的数据库系统来说，内存管理的效率高低是影响系统性能的关键因素。传统C语言中的malloc/free函数是最常用的分配和释放内存的方法，但是这种方法存在着很大的缺陷：

首先对于开发人员来说不匹配的malloc和free容易造成内存泄露；其次频繁调用会造成大量内存碎片无法回收重新利用，降低内存利用率；

然后作为系统调用，其系统开销远远大于一般函数调用。所以，为了提高内存的管理效率，高效的内存管理方案都不会直接使用malloc/free调用。

Memcached默认使用**Slab Allocation**机制管理内存，其主要思想是按照预先规定的大小，将分配的内存分割成特定长度的块以存储相应长度的key-value数据记录，以完全解决内存碎片问题。

Slab Allocation机制只为**存储外部数据**而设计，也就是说所有的key-value数据都存储在Slab Allocation系统里，而Memcached的其它内存请求则通过普通的malloc/free来申请，因为这些请求的数量和频率决定了它们不会对整个系统的性能造成影响。

## **Memcached的Slab Allocation机制**

 ![img](/Users/arpatnurmamat/CS/HomeBase/Note/Redis/assets/out-20240208143635662.png)

如图所示，它**首先从操作系统申请一大块内存，并将其分割成各种尺寸的块Chunk，并把尺寸相同的块分成组Slab Class**。其中，Chunk就是用来存储key-value数据的最小单位。每个Slab Class的大小，可以在Memcached启动的时候通过制定Growth Factor来控制。假定图中Growth Factor的取值为1.25，如果第一组Chunk的大小为88个字节，第二组Chunk的大小就为112个字节，依此类推。

当Memcached接收到客户端发送过来的数据时首先会根据收到数据的大小选择一个最合适的Slab Class，然后通过查询Memcached保存着的该Slab Class内空闲Chunk的列表就可以找到一个可用于存储数据的Chunk。当一条数据库过期或者丢弃时，该记录所占用的Chunk就可以回收，重新添加到空闲列表中。

从以上过程我们可以看出**Memcached的内存管理制效率高，而且不会造成内存碎片，但是它最大的缺点就是会导致空间浪费**。因为每个Chunk都分配了特定长度的内存空间，所以变长数据无法充分利用这些空间。比如将100个字节的数据缓存到128个字节的Chunk中，剩余的28个字节就浪费掉了。Memcached主要的cache机制是LRU（最近最少使用Least Recently Used,具体操作参考:[常见页面置换算法图解](https://www.cnblogs.com/Courage129/p/14308698.html)）算法+超时失效。

## **Redis内存分配机制**

**Redis的内存管理主要通过源码中zmalloc.h和zmalloc.c两个文件来实现的**。Redis为了方便内存的管理，在分配一块内存之后，会将这块内存的大小存入内存块的头部。

 ![img](/Users/arpatnurmamat/CS/HomeBase/Note/Redis/assets/out-20240208143649556.png)

如图所示，real_ptr是redis调用malloc后返回的指针。redis将内存块的大小size存入头部，size所占据的内存大小是已知的，为size_t类型的长度，然后返回ret_ptr(也就是内存大小)。

当需要释放内存的时候，ret_ptr被传给内存管理程序。通过ret_ptr，程序可以很容易的算出real_ptr的值，然后将real_ptr传给free释放内存。

Redis通过定义一个数组来记录所有的内存分配情况，这个数组的长度为ZMALLOC_MAX_ALLOC_STAT。数组的每一个元素代表当前程序所分配的内存块的个数，且内存块的大小为该元素的下标。在源码中，这个数组为zmalloc_allocations。zmalloc_allocations[16]代表已经分配的长度为16bytes的内存块的个数。zmalloc.c中有一个静态变量used_memory用来记录当前分配的内存总大小。

所以，总的来看，**Redis采用的是包装的mallc/free，相较于Memcached的内存管理方法来说，要简单很多**。