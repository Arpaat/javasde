# Redis Set 集合类型

[Redis](https://cloud.tencent.com/product/crs?from_column=20065&from=20065)的Set是string类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据。 Redis 中 集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是O(1)。