# Redis实现分布式锁

## 分布式锁

控制分布式系统不同进程共同访问共享资源的一种锁的实现。如果不同的系统或同一个系统的不同主机之间共享了某个临界资源，往往需要互斥来防止彼此干扰，以保证一致性。

分布式锁的特性：

![img](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/42884a1613344c11be5fef3b9e8ed7c5~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

## Redis分布式锁实现

### 实现一：SETNX+EXPIRE

先用setnx[^1]抢锁，如果抢到锁在用expire给锁设置一个过期时间，防止忘记释放锁

- 加锁命令：SETNX key value，当键不存在时，对键进行设置操作并返回成功（1），否则返回失败（0）。KEY 是锁的唯一标识，一般按业务来决定命名。
- 解锁命令：DEL key，通过删除键值对释放锁，以便其他线程可以通过 SETNX 命令来获取锁。
- 锁超时：EXPIRE key timeout, 设置 key 的超时时间，以保证即使锁没有被显式释放，锁也可以在一定时间后自动释放，避免资源被永远锁住。

则加锁解锁伪代码如下：

```java
if (setnx(key, 1) == 1){
    expire(key, 30)
    try {
        //TODO 业务逻辑
    } finally {
        del(key)
    }
}
```

缺点：setnx和expire两个操作并不是原子性的，线程不安全

### 实现二：SETNX+value值系统时间+过期时间

在setnx获取锁时，将过期时间加到setnx的value中，比较系统时间和过期时间对锁进行释放操作，避免发生异常导致无法释放锁。

缺点：

过期时间是客户端自己生成的（System.currentTimeMillis()是当前系统的时间），必须要求分布式环境下，每个客户端的时间必须同步。

如果锁过期的时候，并发多个客户端同时请求过来，都执行jedis.getSet()，最终只能有一个客户端加锁成功，但是该客户端锁的过期时间，可能被别的客户端覆盖

该锁没有保存持有者的唯一标识，可能被别的客户端释放/解锁。



### 实现三：使用Lua脚本

```java
if (redis.call('setnx', KEYS[1], ARGV[1]) < 1)
then return 0;
end;
redis.call('expire', KEYS[1], tonumber(ARGV[2]));
return 1;

// 使用实例
EVAL "if (redis.call('setnx',KEYS[1],ARGV[1]) < 1) then return 0; end; redis.call('expire',KEYS[1],tonumber(ARGV[2])); return 1;" 1 key value 100
```



### 实现四：SET的扩展命令：set ex px nx

> SET key value[EX seconds][PX milliseconds][NX|XX]
>
> - NX :表示key不存在的时候，才能set成功，也即保证只有第一个客户端请求才能获得锁，而其他客户端请求只能等其释放锁，才能获取。
> - EX seconds :设定key的过期时间，时间单位是秒。
> - PX milliseconds: 设定key的过期时间，单位为毫秒
> - XX: 仅当key存在时设置值



## Redis分布式锁存在问题

1. setnx，expire 非原子性问题

2. 锁误解除，其他线程解除了锁

3. 超时解锁导致并发

4. 不可重入

5. 无法等待锁释放

    当获取锁失败时，就返回。

    * 通过轮询方式，多次尝试获取锁，直到获取锁或超时。比较消耗系统性能
    * 使用Redis发布订阅功能，获取锁失败就订阅锁释放消息，当获取锁成功并释放锁时，发布锁释放消息



## 代码解析

nx:

ex:

```java
private boolean setNx(String key, long expireSeconds, PersistenceRouter router) {
        try {
            String status = clusterHolder.getSyncCommands(router)
                    .set(key, "lock", SetArgs.Builder.nx().ex(expireSeconds));
            /* status为 OK ： 设置成功， key不存在
             * status为null： 设置失败， key已存在
             * */
            return StringUtils.equals(SUCCESS_STATUS, status);
        } catch (Exception e) {
            logger.error("query consistentKey exception! key: {}", key, e);
        }
        return false;
    }
```



## 文档

https://mp.weixin.qq.com/s/qJK61ew0kCExvXrqb7-RSg

https://xiaomi-info.github.io/2019/12/17/redis-distributed-lock/

https://juejin.cn/post/6936956908007850014