# OOM

OOM：指out of memory , java.lang.OutofMemoryError. 当jvm没有足够内存给对象分配空间时就会抛出oom。

两个概念：

* 内存泄露：当内存使用完毕后没有被回收，导致不能再次使用这块内存，这块内存就会泄露
* 内存溢出：当申请内存超出了jvm能提供的内存大小时，称为溢出

## 常见的oom场景

1. java.lang.outofmemoryerror:java heap space 堆内存溢出
2. java.lang.outofmemoryerror: PermGen space 永久代溢出
3. Stackoverflow 栈溢出









## 文档

https://blog.csdn.net/wan212000/article/details/121659189