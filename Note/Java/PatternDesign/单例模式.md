#  设计模式

设计模式主要分为：创建型模式、行为型模式、结构型模式

## **单例模式**

### **定义**

单例模式只涉及一个类，确保在系统中一个类只有一个实例，并提供一个全局访问入口。

### 使用场景

日志类、配置类、工厂类、以共享模式访问资源的类、在Spring中创建的Bean实例默认都是以单例模式存在的

* 需要生成唯一序列的环境
* 频繁实例化并销毁的对象
* 创建对象消耗时间、资源过多但是频繁使用的对象
* 方便资源相互通信的环境

### **特点**

#### **优点：**

- 在内存中只有一个对象，节省内存空间；
- 避免频繁的创建销毁对象，减轻 GC 工作，同时可以提高性能；
- 避免对共享资源的多重占用，简化访问；
- 为整个系统提供一个全局访问点。

#### **缺点：**

- 不适用于变化频繁的对象；
- 滥用单例将带来一些负面问题，如为了节省资源将数据库连接池对象设计为的单例类，可能会导致共享连接池对象的程序过多而出现连接池溢出；
- 如果实例化的对象长时间不被利用，系统会认为该对象是垃圾而被回收，这可能会导致对象状态的丢失；



### **单例模式的实现**

#### **1. 饿汉式：线程安全**

饿汉式就是指在类加载时就创建对象，但是容易产生垃圾对象，消耗内存。其特点是：线程安全、没有加锁因此执行效率高，但是类加载时就初始化，浪费内存。

饿汉式线程安全实现机制：基于类加载机制避免多线程的同步问题，但如果类被不同的类加载器加载就会创建不同的实例。

```Java
public class Singleton {
    //私有化构造器
    private Singleton(){}
    
    //定义一个静态变量指向自己
    private static final Singleton instance = new Singleton();
    
    //向外提供一个获取实例的公共方法
    public static Singleton getInstance(){
        return instance;
    }
}
```

#### 2. **懒汉式：线程不安全**

在多线程情况下容易破坏单例，在多线程环境下无法保障单例。

```Java
public class Singleton2 {
    //私有化构造器
    private Singleton2(){}
    //定义一个静态变量指向单例，没有直接创建实例
    private static Singleton2 instance;
    //对外提供一个公共方法获取单例，在调用getInstance方法时才创建实例
    public static Singleton2 getInstance(){
        if (instance == null){
            instance = new Singleton2();
        }
        return instance;
    }   
}
```

#### 3. **懒汉式：线程安全**

通过synchronized关键字保障线程安全，synchronized可以添加到方法或代码块上，在每次获取实例时需要经历加锁和释放锁，性能影响较大

```Java
public class Singleton3 {
    //
    private Singleton3(){}
    
    private static Singleton3 instance;

    /**
     * 对获取实例方法进行加锁，线程每次调用该方法都需要获取锁
     * @return
     */
    public synchronized static Singleton3 getInstance(){
        if (instance == null){
            instance = new Singleton3();
        }
        return instance;
    }
}
```

#### 4. **双重检查锁：DCL**-Double Checking Locking

懒加载、线程安全、效率较高但是实现比较复杂。第一重判断，如果实例存在则直接返回实例，如果没有则进入同步代码块，为了防止多线程同时调用，使用synchronized进行加锁。**关于第二重判断，当第二重为null时，创建对象，但是new创建对象过程为非原子性操作，因此可能在操作过程中，有其他线程调用方法并且到达第一重判断并返回instance ！= null，并直接返回还未创建完成的instance，导致出现问题，因此需要添加volatile关键字禁止指令重排序。**

```Java
public class Singleton4 {
    private Singleton4(){}

    private static volatile Singleton4 instance;

    public static Singleton4 getInstance(){
        //第一重检查
        if (instance == null){
            //使用synchronized加锁
            synchronized (Singleton4.class){
                //第二重检查
                if (instance == null){
                    instance = new Singleton4();
                }
            }
        }
        return instance;
    }
}
```

#### 5. **静态内部类**

懒加载、线程安全、效率较高、实现简单

```java
public class Singleton {
  private Singleton(){}
  
  public static Singleton getInstance(){
    	return InnerClass.INSTANCE;
  }
  
  //定义静态内部类
  private static class InnerClass{
    	private final static Singleton INSTANCE = new Singleton();
  }
}
```

静态内部类方式如何实现懒加载？

虚拟机规范有且只有以下方式必须立即对类进行初始化

1. 遇到new, getstatic, putstatic, invoke static 字节码指令时
2. 使用java.lang.reflect 方法对类进行反射调用
3. 当初始化一个类发现其父类还未被初始化
4. 当虚拟机启动，用户需指定一个主类，（有main（）方法的类）
5. JDK17的动态语言支持时，`java.lang.invoke.MethodHandle` `实例最后的解析结果是` `REF_getStatic`、`REF_putStatic`、`REF_invokeStatic` 的方法句柄，则需要先触发这个方法句柄所对应的类的初始化。

这5种类型被称为**类的主动引用**，静态内部类属于类的被动引用。只有在getInstance方法调用时，InnerClass才会到Singleton的运行时常量池中，把符号引用替换为直接引用，这时静态对象 INSTANCE 也真正被创建，然后再被 getInstance()方法返回出去，这点同饿汉模式。

那么 `INSTANCE` 在创建过程中又是如何保证线程安全的呢？在《深入理解JAVA虚拟机》中，有这么一句话:

 虚拟机会保证一个类的 `<clinit>()` 方法在多线程环境中被正确地加锁、同步，如果多个线程同时去初始化一个类，那么只会有一个线程去执行这个类的 `<clinit>()` 方法，其他线程都需要阻塞等待，直到活动线程执行 `<clinit>()` 方法完毕。如果在一个类的 `<clinit>()` 方法中有耗时很长的操作，就可能造成多个进程阻塞(**需要注意的是，其他线程虽然会被阻塞，但如果执行`<clinit>()`方法后，其他线程唤醒之后不会再次进入`<clinit>()`方法。同一个加载器下，一个类型只会初始化一次。**)，在实际应用中，这种阻塞往往是很隐蔽的。



#### 6. **枚举单例**

```java
public enum Singleton {
  	INSTANCE;
  	public void doSomething(String str) {
      	Sout(str);
    }
}
//调用
Singleton singleton = Singleton.INSTANCE;
```

从枚举的反编译结果可以看到，INSTANCE 被 `static final `修饰，所以可以通过类名直接调用，**并且创建对象的实例是在静态代码块中创建的**，因为 static 类型的属性会在类被加载之后被初始化，当一个Java类第一次被真正使用到的时候静态资源被初始化、Java类的加载和初始化过程都是线程安全的，所以创建一个enum类型是线程安全的。

如果Singleton需要继承其他类，则无法使用枚举类创建单例

**使用单例设计模式需要注意的点：**

- **多线程- 在多线程应用程序中必须使用单例时，应特别小心。**
- **序列化- 当单例实现 Serializable 接口时，他们必须实现 readResolve 方法以避免有 2 个不同的对象**。
- **类加载器- 如果 Singleton 类由 2 个不同的类加载器加载，我们将有 2 个不同的类，每个类加载一个。**
- **由类名表示的全局访问点- 使用类名获取单例实例。这是一种访问它的简单方法，但它不是很灵活。如果我们需要替换Sigleton类，代码中的所有引用都应该相应地改变。**

