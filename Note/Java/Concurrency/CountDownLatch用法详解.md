# CountDownLatch用法详解

CountDownLatch是具有synchronized机制的一个工具，目的是让一个或者多个线程等待，直到其他线程的一系列操作完成。CountDownLatch初始化的时候，需要提供一个整形数字，数字代表着线程需要调用countDown()方法的次数，当计数为0时，线程才会继续执行await()方法后的其他内容。CountDownLatch(int count);

```java
getCount：
返回当前的计数count值，
```

```java
public void countDown()
调用此方法后，会减少计数count的值。
递减后如果为0，则会释放所有等待的线程
```

```java
public void await()
           throws InterruptedException
调用CountDownLatch对象的await方法后。
会让当前线程阻塞，直到计数count递减至0。
```

## demo

```java
public class cdlDemo1 {
    public static void main(String[] args) {
        final CountDownLatch downLatch = new CountDownLatch(1);
        new Thread(){
            public void run(){
                try {
                    System.out.println("child thread " + Thread.currentThread().getName() + " running ");
                    Thread.sleep(1000);
                    System.out.println("child thread " + Thread.currentThread().getName() + " finish ");
                    downLatch.countDown();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }.start();

        new Thread() {
            public void run(){
                try {
                    System.out.println("child thread " + Thread.currentThread().getName() + " running ");
                    Thread.sleep(1000);
                    System.out.println("child thread " + Thread.currentThread().getName() + " finish ");
                    downLatch.countDown();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }.start();
        try {
            System.out.println(" Waiting for two child thread stop... ");
            downLatch.await();
            System.out.println(" two child thread finish ");
            System.out.println(" continue main thread ");
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}

```

countDownLatch.countDown(); 这一句话尽量写在finally中，或是保证此行代码前的逻辑正常运行，因为在一些情况下，出现异常会导致无法减一，然后出现死锁。

CountDownLatch 是一次性使用的，当计数值在构造函数中初始化后，就不能再对其设置任何值，当 CountDownLatch 使用完毕，也不能再次被使用。



## 文档

https://segmentfault.com/a/1190000021696494

