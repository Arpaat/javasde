> CAP理论表达了一个分布式系统里不可能同时满足三种特性：数据一致性、可用性、分区容忍性

一图胜千言：

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAC994bxI0cYxev6mJONjIFG:-2192221264303603915fcAC994bxI0cYxev6mJONjIFG:1707375843481)

# **Consistency：数据一致性**

指所有节点在同一时间的数据完全一致。一致性是因为多个数据拷贝下并发读写才有的问题，因此理解时一定要注意结合考虑多个数据拷贝下并发读写的场景。

一致性的分类有：强一致性、弱一致性、最终一致性

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAC994bxI0cYxev6mJONjIFG:8208404416460772792fcAC994bxI0cYxev6mJONjIFG:1707375843481)

# **Availability：可用性**

指服务在正常响应时间内一直可用

# **Partition Tolerance： 分区容错性**

即分布式系统在遇到某节点或网络分区故障的时候，仍然能够对外提供满足一致性或可用性的服务。

# **相关文档**

https://cloud.tencent.com/developer/article/1860632