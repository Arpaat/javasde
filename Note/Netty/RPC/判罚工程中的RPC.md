# 判罚工程中的RPC

### 在Draco工程中定义proto文件

```protobuf
syntax = "proto3";

import "common/draco_common_params.proto";

package kuaishou.draco.aquila.rpc;

option java_package = "com.kuaishou.protobuf.draco.aquila.rpc";
option java_multiple_files = true;

message AquilaBatchTopicProto {
    AquilaBatchRequest batch_request = 1;
    int32 punish_type = 2; // 处罚类型：： 1：处罚、2：恢复
}

// 判罚批量请求接口
message AquilaBatchRequest {
    repeated AquilaRequest aquila_request = 1;
    string trace_id = 2; //
    string kpn = 3; // 产品线（如：KUAISHOU）
    string sub_biz = 4; // 业务场景（天龙中kpn+subBiz确定一个业务场景）
    string event_type = 5; // 事件类型（业务场景，风控系统中的业务标识）
    int64 timestamp = 6; // 请求时间
}

enum AquilaResult {
    ALLOW = 0; //判决允许(透传)
    REJECT = 1; //判决拒绝
    CHANGE = 2; //判决更改
    SECOND_CHECK = 3; // 二次确认
}

message AquilaTopicProto {
    AquilaRequest aquila_request = 1;
    int32 punish_type = 2; // 处罚类型：： 1：处罚、2：恢复
}

message AquilaRequest {
    com.kuaishou.draco.common.message.RequestCommonParams common_params = 1; // 公共参数
    string punish_target_id = 2; // 判罚对象ID
    int32 punish_target_type = 3; // 判罚对象类型（参考com.kuaishou.draco.common.constants.TargetType.java）
    string punish_reason = 4;
    int64 auditor_id = 5; // 审核员ID
    int64 audit_time = 6; // 审核时间
    string desc = 7; // 审核备注
    string policy_id = 8; // 策略ID(风控才传)
    int32 punish_code = 9; // 处置编码
    map<string, string> punish_params = 10; // 惩罚参数
    map<string, string> extra_params = 11; // 额外参数
    string relation_id = 12; // 关联对象ID（用于标记关联判罚）
    int32 relation_target_type = 13; // 关联对象类型（参考com.kuaishou.draco.common.constants.TargetType.java）
    string policy_update_user = 14; // 策略负责人(风控才传)
}

// 同步响应
message AquilaResponse {
    com.kuaishou.draco.common.message.ResponseCommonParams common_params = 1; // 公共参数
    AquilaResult aquila_result = 2; // 判罚结果
    string aquila_reason = 3; // 原因
}

// 异步通知（kafka）
message AquilaNotifyMessage {
    com.kuaishou.draco.common.message.NotifyMessageCommonParams common_params = 1; // 公共参数
    AquilaResult aquila_result = 2; // 判罚结果
    string aquila_reason = 3; // 原因
}

service AquilaRpcService {
    // 判罚
    rpc Process (AquilaRequest) returns (AquilaResponse);
}

```

其中定义的message经过编译后会生成对应的java类。具体位置在target/generated-sources/protobuf/...

### 在draco工程中定义Client类，作为服务调用的接口 -> 解决如何调用问题



### 在aquila工程中实现具体的服务内容 -> 解决要做什么问题？









