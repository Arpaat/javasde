# RPC框架

## 介绍

### 什么是RPC框架

RPC(Remote Procedure Call Protocol)远程过程调用协议。一个通俗的描述是：客户端在不知道调用细节的情况下，调用存在于远程计算机上的某个对象，就像调用本地应用程序中的对象一样。比较正式的描述是：**一种通过网络从远程计算机程序上请求服务，而不需要了解底层网络技术的协议。**需要注意对几个点：

- RPC是一个协议，具有一套规范
- 网络协议和网络IO模型对其透明（透明是指无需考虑/看不到）
- 信息格式对其透明

rpc过程调用模型

 ![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAA-z6_oyCJcxDhqtUxIRtM6:4708405545124088093fcAA-z6_oyCJcxDhqtUxIRtM6:1691593295255)

一个完整rpc框架包含四个核心组件：client, server, client stub, server stub，这个stub可以理解为‘存根’（调用与返回）。

* client：服务调用者
* server：服务提供者
* client stub：存放服务端的地址消息，再将client的请求参数打包成网络信息，网络发送给服务方
* server stub：接受客户端发送的消息，将消息解包并调用本地方法

### **应用场景**

当多个服务在不同当机器上运行时，需要互相调用，因此rpc提供应用程序之间的高效的通讯手段。

### **rpc原理**

1. 调用流程

 ![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAA-z6_oyCJcxDhqtUxIRtM6:-3841659985875007732fcAA-z6_oyCJcxDhqtUxIRtM6:1691593315341)

- client stub接收到调用后负责将方法、参数等组装成能网络传输的消息体 message 形式
- client stub找到服务地址并发送给服务端
- server stub接收后进行解码，并根据解码结果调用本地服务
- 本地服务执行并返回结果给server stub，server stub负责将其封装成消息体并发送给client

1. 对消息进行编码和解码

2.1 确定消息数据结构

客户端请求消息结构一般为：

- 接口名称
- 方法名称
- 参数类型&参数值
- 超时时间+requestID（标示唯一请求ID）

服务端返回消息结构

- 状态code+返回值
- requestID

### 常用的RPC框架

* gRPC
* Dubbo
* Spring Cloud

## 动态代理

> 如何实现透明化远程服务调用？

对于Java就是使用代理，通常有两种代理方式，（1）jdk动态代理 （2）字节码生成，此处使用jdk动态代理

**静态代理：每个代理类只能为一个接口服务，这样会产生很多代理类。**普通代理模式，代理类Proxy的Java代码在JVM运行时就已经确定了，也就是**静态代理在编码编译阶段就确定了Proxy类的代码**。而**动态代理是指在JVM运行过程中，动态的创建一个类的代理类，并实例化代理对象**

JDK 动态代理是利用反射机制生成一个实现代理接口的匿名类，在调用业务方法前调用`InvocationHandler` 处理。代理类必须实现 `InvocationHandler` 接口，并且，JDK 动态代理只能代理实现了接口的类

**JDK 动态代理类基本步骤：**

- 编写需要被代理的类和接口
- 编写代理类，需要实现 `InvocationHandler` 接口，重写 `invoke()` 方法；
- 使用`Proxy.newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h)`动态创建代理类对象，通过代理类对象调用业务方法。

动态代理小Demo

```java
// 1、创建代理对象的接口
interface DemoInterface {
    String hello(String msg);
}
// 2、创建具体被代理对象的实现类
class DemoImpl implements DemoInterface {
    @Override
    public String hello(String msg) {
        System.out.println("msg = " + msg);
        return "hello";
    }
}
// 3、创建一个InvocationHandler实现类，持有被代理对象的引用，invoke方法中：利用反射调用被代理对象的方法
class DemoProxy implements InvocationHandler {
    private DemoInterface service;
    public DemoProxy(DemoInterface service) {
        this.service = service;
    }
    @Override
    public Object invoke(Object obj, Method method, Object[] args) throws Throwable {
        System.out.println("调用方法前...");
        Object returnValue = method.invoke(service, args);
        System.out.println("调用方法后...");
        return returnValue;
    }
}
public class Solution {
    public static void main(String[] args) {
        DemoProxy proxy = new DemoProxy(new DemoImpl());
        //4.使用Proxy.newProxyInstance(ClassLoader loader, Class<?>[] interfaces, InvocationHandler h)动态创建代理类对象，通过代理类对象调用业务方法。
        DemoInterface service = (DemoInterface)Proxy.newProxyInstance(
            DemoInterface.class.getClassLoader(),
            new Class<?>[]{DemoInterface.class},
            proxy
        );
        System.out.println(service.hello("呀哈喽！"));
    }
}
```

newProxyInstance:

- loader: 用哪个类加载器去加载代理对象
- interfaces:动态代理类需要实现的接口
- **h:动态代理方法在执行时，会调用h里面的invoke方法去执行**

invoke三个参数：

- obj：就是代理对象，newProxyInstance方法的返回对象
- method：调用的方法
- args: 方法中的参数

## 序列化和反序列化

> 对象如何在网络中传输？

**通过将对象序列化成字节数组，即可将对象发送到网络中。**在 Java 中，想要序列化一个对象：

- **要序列化对象所属的类必须实现了 `Serializable` 接口**；
- 并且其内部属性必须都是可序列化的

**序列化对象主要由两种用途：**

- **把对象的字节序列永久地保存到硬盘上**，通常存放在一个文件中或数据库中；
    比如最常见的是Web服务器中的Session对象，当有 10万用户并发访问，就有可能出现10万个Session对象，内存可能吃不消，于是Web容器就会把一些session先序列化到硬盘中，等要用了，再把保存在硬盘中的对象还原到内存中。
- **用于网络传输对象的字节序列。**
    当两个进程在进行远程通信时，彼此可以发送各种类型的数据。无论是何种类型的数据，都会以二进制序列的形式在网络上传送。发送方需要把这个Java对象转换为字节序列，才能在网络上传送；接收方则需要把字节序列再恢复为Java对象。

> Java序列化时，哪些变量不会被序列化？

**序列化期间，静态变量（static修饰）和瞬态变量（transient修饰）未被序列化：**

- 由于静态变量属于类，而不是对象，**序列化保存的是对象的状态，静态变量保存的是类的状态**。因此在 Java 序列化过程中不会保存它们。**注意：static修饰的变量是不可序列化，同时也不可串行化。**
- 瞬态变量也不包含在 Java 序列化过程中, 并且不是对象的序列化状态的一部分。

**如果有一个属性（字段）不想被序列化的，则该属性必须被声明为 `transient`**！

- 一旦变量被transient修饰，变量将不再是对象持久化的一部分，该变量内容在序列化后无法被访问（如银行卡号、密码等不想用序列化机制保存）。
- **transient关键字只能修饰变量，而不能修饰方法和类**。注意，本地变量是不能被transient关键字修饰的。变量如果是用户自定义类变量，则该类需要实现Serializable接口。

> serialVersionUID的作用

Java的序列化机制是**通过 在运行时 判断类的serialVersionUID来验证版本一致性的（用于实现对象的版本控制）。**serialVersionUID 是一个 private static final long 型 ID, 当它被印在对象上时, 它通常是对象的哈希码，你可以使用 serialver 这个 JDK 工具来查看序列化对象的 serialVersionUID。

在进行反序列化时，JVM会把传来的字节流中的serialVersionUID与本地相应实体（类）的serialVersionUID进行比较，如果相同就认为是一致的，可以进行反序列化，否则就会出现序列化版本不一致的异常。(**InvalidCastException**)。**类的serialVersionUID的默认值完全依赖于Java编译器的实现**，对于同一个类，用不同的Java编译器编译，有可能会导致不同的 serialVersionUID，也有可能相同。**为了提高serialVersionUID的独立性和确定性，强烈建议在一个可序列化类中显示的定义serialVersionUID，为它赋予明确的值**。

显式地定义serialVersionUID有两种用途：

- 在某些场合，希望类的不同版本对序列化兼容，因此需要确保类的不同版本具有相同的serialVersionUID；
- 在某些场合，不希望类的不同版本对序列化兼容，因此需要确保类的不同版本具有不同的serialVersionUID。



**如何解决获取实例的问题**？既然系统采用分布式架构，那一个服务势必会有多个实例，所以需要**一个服务注册中心**，比如在Dubbo中，就可以使用Zookeeper作为注册中心，在调用时，从**Zookeeper**获取服务的实例列表，再从中选择一个进行调用。也可以同**Nacos**做服务注册中心；

**如何选择实例？**就要考虑**负载均衡**，例如dubbo提供了4种负载均衡策略；

如果每次都去注册中心查询列表，效率很低，那么就要加**缓存**；

客户端总不能每次调用完都等着服务端返回数据，所以就要支持**异步调用**；

服务端的接口修改了，老的接口还有人在用，这就需要**版本控制**；

服务端总不能每次接到请求都马上启动一个线程去处理，于是就需要**线程池**；



## **参考文档**

https://halo.corp.kuaishou.com/help/docs/f198cbc77d34302b510415b678a00b6e

https://git.corp.kuaishou.com/infra/infra-krpc/blob/master/doc/krpc-design.md

快手中学：RPC课程https://school.corp.kuaishou.com/school/web/play/KC2020153#section=988

https://www.51cto.com/article/701423.html