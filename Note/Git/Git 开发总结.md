# Git 开发总结

---

## 开发流程

主要采用master分支发布，对分支进行开发并完成上线测试，测试完成后merge到master中。每个分支开发完成后在push前需要提交kdev进行Code Review。

![Git工作流程](https://www.runoob.com/wp-content/uploads/2015/02/git-process.png)

## Git 基本操作

![img](https://www.runoob.com/wp-content/uploads/2015/02/git-command.jpg)

### 创建仓库命令

下表列出了 git 创建仓库的命令：

| 命令        | 说明                                   |
| :---------- | :------------------------------------- |
| `git init`  | 初始化仓库                             |
| `git clone` | 拷贝一份远程仓库，也就是下载一个项目。 |

------

### 提交与修改

Git 的工作就是创建和保存你的项目的快照及与之后的快照进行对比。

下表列出了有关创建与提交你的项目的快照的命令：

| 命令         | 说明                                     |
| :----------- | :--------------------------------------- |
| `git add`    | 添加文件到暂存区                         |
| `git status` | 查看仓库当前的状态，显示有变更的文件。   |
| `git diff`   | 比较文件的不同，即暂存区和工作区的差异。 |
| `git commit` | 提交暂存区到本地仓库。                   |
| `git reset`  | 回退版本。                               |
| `git rm`     | 将文件从暂存区和工作区中删除。           |
| `git mv`     | 移动或重命名工作区文件。                 |

### 提交日志

| 命令               | 说明                                 |
| :----------------- | :----------------------------------- |
| `git log`          | 查看历史提交记录                     |
| `git blame <file>` | 以列表形式查看指定文件的历史修改记录 |

### 远程操作

| 命令         | 说明               |
| :----------- | :----------------- |
| `git remote` | 远程仓库操作       |
| `git fetch`  | 从远程获取代码库   |
| `git pull`   | 下载远程代码并合并 |
| `git push`   | 上传远程代码并合并 |



## Git 分支管理

1. 分支创建： `git branch <branchName>`
2. 分支切换：`git checkout <branchname>`
3. 合并分支：`git merge <branchname>`

如果在分支a进行开发，需要将master分支代码合并到分支a上，即可 切换到分支a上 再运行`git merge master `

4. 列出分支：`git branch`

5. 删除分支：`git branch -d <branchname>`



1. Git 推送本地分支到远程分支

开发中，一般有两种分支创建方式，在远程创建并拉到本地和本地创建推送到远程

远程先开好分支然后拉到本地：

```armasm
git checkout -b feature-branch origin/feature-branch    //检出远程的feature-branch分支到本地
```

本地先开好分支然后推送到远程：

```armasm
$  git checkout -b feature-branch    //创建并切换到分支feature-branch  
$  git push origin feature-branch:feature-branch    //推送本地的feature-branch(冒号前面的)分支到远程origin的feature-branch(冒号后面的)分支(没有会自动创建)
```