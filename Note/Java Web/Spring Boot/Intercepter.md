## **什么是拦截器Interceptor？**

### **定义**

> Spring Boot Interceptor is an additional component that will intercept every request and response dispatch and perform some operations on it

拦截器主要作用是对controller进行预处理和后处理，类似于Servlet中的Filter，主要基于Java反射机制，属于AOP的一种应用

### **特点**

- 请求和相应都会经过拦截器
- 只能拦截kcontroller相关的请求
- 拦截器可以中断用户请求轨迹

### **如何使用**

实现HandleInterceptor接口或实现WebRequestInterceptor

## **什么是过滤器Filter？**

> Spring Boot filters are objects that intercept and process incoming HTTP requests and outgoing HTTP responses. They are executed in a specific order, allowing each filter to perform its designated task. Once all the filters have been executed, the request is passed on to the appropriate controller or handler. Filters are commonly used for tasks such as authentication, logging, and request/response modification.

原理图：

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcADD4ilgCZRK847AS5uYrGez:-110001691688797806fcADD4ilgCZRK847AS5uYrGez:1707375783032)

## **拦截器工作原理**

原理图：

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcADD4ilgCZRK847AS5uYrGez:-708482283773510225fcADD4ilgCZRK847AS5uYrGez:1707375783033)

- preHandle方法在请求处理之前进行调用
- AfterCompletion方法在整个请求结束之后，也就是DispatcherServlet渲染对应视图之后执行，主要进行资源清理工作
- PostHandle方法在请求处理之后，在DispatcherServlet渲染之前调用