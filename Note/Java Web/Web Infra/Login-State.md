## **什么是登陆态？**

目前主流的web应用是基于HTTP协议的，而HTTP协议是*无状态的，*无状态是指服务器不知道请求是从哪里发送，无法区别用户身份。因此**登陆态就是服务器用来识别用户的身份，同时对用户进行记录的技术方案**。

实现登陆态的流程：

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcABs66IOC1Ymb6YccFsicE0z:-6276688962702173162fcABs66IOC1Ymb6YccFsicE0z:1707375618561)

## **实现方案：****HTTP基本认证、Cookie和Session认证、Token认证、单点登录认证**

1. ### **HTTP基本认证**

HTTP基本认证是HTTP协议本身提供了一种服务端对客户端进行用户身份验证的方法。 流程如下：

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcABs66IOC1Ymb6YccFsicE0z:-7812066674108585991fcABs66IOC1Ymb6YccFsicE0z:1707375618561)



```
sequenceDiagram
autonumber
客户端->>服务端:Get / HTTP/1.1 Host:www.qq.com
服务端->>客户端:HTTP/1.1 401 Unauthorised WWW-Authenticate: Basic realm="qq.com"
客户端->>客户端:弹出登录窗口
客户端->>服务端:Get / HTTP/1.1 Host:www.qq.com Authorization: Basic xxxxxx
服务端->>客户端:验证成功，返回用户数据
```



1. 客户端向服务端请求需要登录态的数据
2. 服务端向客户端返回401状态码，要求客户端验证
3. 客户端根据返回的 WWW-Authenticate: Basic realm="[qq.com](http://qq.com)"，弹出用户名和密码输入框要求用户进行验证。
4. 用户输入用户名和密码后，客户端将用户名及密码以 Base64 格式发送给服务端。
5. 服务端验证通过后返回用户数据。

这是一种比较简单的验证用户身份的方式，甚至不需要写代码，只要后端服务器配置一下即可。

优点：兼容性好，主流浏览器都支持

缺点：

- 不安全，账号密码是Base64编码，很容易解码。
- 无法主动注销，除非关闭标签或浏览器。

1. ### **Cookie和Session认证**

Cookie和Session是弥补HTTP协议不状态的方案

#### **2.1 什么是Cookie**

**Cookie是客户端请求服务端时，由服务端创建并由客户端存储和管理的小文本文件**。Cookie的创建流程如下：

- 客户端首次发起请求。
- 服务端通过HTTP响应头里Set-Cookie字段返回Cookie信息。
- 客户端再发起请求时会通过HTTP请求头里Cookie字段携带Cookie信息

#### **2.2 什么是Session**

**Session是客户端请求服务端时服务端会为这次请求创建一个数据结构**，这个结构可以通过内存、文件、数据库等方式保存。具体流程如下：

- 客户端首次发起请求。
- 服务端收到请求并自动为该客户端创建特定的Session并返回SessionID，用来标识该客户端。
- 客户端通过服务端响应获取SessionID，并在后续请求携带SessionID。
- 服务端根据收到的SessionID，在服务端找对应的Session，进而获取到客户端信息。

#### **2.3 Cookie和Session的认证流程**

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcABs66IOC1Ymb6YccFsicE0z:2465151451810121913fcABs66IOC1Ymb6YccFsicE0z:1707375618562)

1. 客户端向服务端发送认证信息（例如账号密码）
2. 服务端根据客户端提供的认证信息执行验证逻辑，如果验证成功则生成Session并保存，同时通过响应头**Set-Cookie字段**返回对应的SessionID
3. 客户端再次请求并在Cookie里携带SessionID。
4. 服务端根据SessionID查找对应的Session，并根据业务逻辑返回相应的数据。

#### **2.4 Cookie和Session认证的优缺点**

优点：

- Cookie由客户端管理，支持设定有效期、安全加密、防篡改、请求路径等属性。
- Session由服务端管理，支持有效期，可以存储各类数据。

缺点：

- Cookie只能存储字符串，有大小和数量限制，对移动APP端支持不好，同时有**跨域限制**（主域不同）。
- Session存储在服务端，对服务端有性能开销，客户端量太大会影响性能。如果集中存储（如存储在Redis），会带来额外的部署维护成本

1. ### **Token认证**

Token又叫令牌，是服务端生成用来验证客户端身份的凭证，客户端每次请求都携带Token。 Token一般由以下数据组成：



```
uid(用户唯一的身份标识)
time(当前时间的时间戳)
sign(签名，由token的前几位+盐用哈希算法压缩成一定长的十六进制字符串)
```



认证流程

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcABs66IOC1Ymb6YccFsicE0z:7115565491018000461fcABs66IOC1Ymb6YccFsicE0z:1707375618563)

#### **3.1 Token认证的优缺点**

优点：

- 客户端可以用Cookie、LocalStorage等存储，服务端不需要存储。
- 安全性高（有签名校验）。
- 支持移动APP端。
- 支持跨域。

缺点：

- 占用额外传输宽带，因为Token比较大，可能会消耗一定的流量。
- 每次签名校验会消耗服务端性能。
- 有效期短（避免被盗用）

1. ### **单点登陆认证 Single Sign On**

SSO的作用是：在多个系统中，只需要登陆一次，就可以访问其他相互信任的应用系统

#### **4.1 不同域下的单点登陆**

CAS(Central Authentication Servcie) 流程如下图

1. 用户访问app系统，app系统是需要登录的，但用户现在没有登录。
2. 跳转到CAS server，即SSO登录系统，**以后图中的CAS Server我们统一叫做SSO系统。** SSO系统也没有登录，弹出用户登录页。
3. 用户填写用户名、密码，SSO系统进行认证后，将登录状态写入SSO的session，浏览器（Browser）中写入SSO域下的Cookie。
4. SSO系统登录完成后会生成一个ST（Service Ticket），然后跳转到app系统，同时将ST作为参数传递给app系统。
5. app系统拿到ST后，从后台向SSO发送请求，验证ST是否有效。
6. 验证通过后，app系统将登录状态写入session并设置app域下的Cookie。

至此，跨域单点登录就完成了。以后我们再访问app系统时，app就是登录的。接下来，我们再看看访问app2系统时的流程。

1. 用户访问app2系统，app2系统没有登录，跳转到SSO。
2. 由于SSO已经登录了，不需要重新登录认证。
3. SSO生成ST，浏览器跳转到app2系统，并将ST作为参数传递给app2。
4. app2拿到ST，后台访问SSO，验证ST是否有效。
5. 验证成功后，app2将登录状态写入session，并在app2域下写入Cookie。

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcABs66IOC1Ymb6YccFsicE0z:-4509276232253169659fcABs66IOC1Ymb6YccFsicE0z:1707375618564)

# **参考文档**

https://zhuanlan.zhihu.com/p/591434948

https://developer.aliyun.com/article/636281

https://blog.csdn.net/y532798113/article/details/106711837