## **什么是跨域问题**

跨域是**指当一个请求url的协议、域名、端口三者之间的任意一个与当前页面url不同即为跨域**。

举个例子：

| **当前页面url**                                              | **被请求页面url**                                            | **是否跨域** | **原因**                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ | ------------------------------ |
| [http://www.testlocation.com/](https://link.segmentfault.com/?enc=1kFPufO%2FYWyw923YTKvewA%3D%3D.p9svHjoRCSPs%2FAZ3jT8%2FXC9KimiTwUcVtSA0VFNWsVo%3D) | [http://www.testlocation.com/index.html](https://link.segmentfault.com/?enc=g60A%2BtPqJo96XmgOmuPYyg%3D%3D.8DqXtV8zo%2FhxOP1XI3je4l2BFzO0klYuH9CMRl6a4X1b8CN5W1bFwXUMwDskqDXb) | 否           | 同源（协议、域名、端口号相同） |
| [http://www.testlocation.com/](https://link.segmentfault.com/?enc=wZqYgnSBDy3FKRxyF8GeSg%3D%3D.yyBMLYeGwPK9PeIteS162xhXijpzA4AgFUjsBsRiavY%3D) | [https://www.testlocation.com/index.html](https://link.segmentfault.com/?enc=K%2BPRhVFQVaHxe5n2%2FoV4Iw%3D%3D.yLEMl9ukg9UUOELTort0WjWXWnWjNihuFBDXbUt7NCalIrsKapUNKh3BMDEqEZAV) | 跨域         | 协议不同（http/https）         |
| [http://www.testlocation.com/](https://link.segmentfault.com/?enc=S8WEPTi%2BXMFoD4%2Fv3b8H%2Fg%3D%3D.OEAfCLSHm0S39LQqCl9JGZR%2FIYlRnVDz0LoZn9uYs5g%3D) | [http://www.baidu.com/](https://link.segmentfault.com/?enc=G82eFQpXTMDvapEyb0O8sw%3D%3D.XxbA2HmBBHnAH1QHWnMUzeFsiiSMsGYiUcgr821dEx4%3D) | 跨域         | 主域名不同（test/baidu）       |
| [http://www.testlocation.com/](https://link.segmentfault.com/?enc=t4cUin51C%2BbTAIXOuxsYfQ%3D%3D.0IdKE5oVCTkxlFzwLEAOHCaf74WZVFp3xUrEoQy7gYY%3D) | [http://blog.testlocation.com/](https://link.segmentfault.com/?enc=wY50BqniosAGc6r%2FgpdXPA%3D%3D.DEi0Yo5gJ1yCKrV7x78yRYv%2Bd7YZkGD5WAPBFov5BoI%3D) | 跨域         | 子域名不同（www/blog）         |
| [http://www.testlocation.com:8080/](https://link.segmentfault.com/?enc=2mvkyYApfzu%2Bs9wxWEBmRQ%3D%3D.Y3J0hZ67a6JMUD9dINq2BP5E263B9SY%2BKnHAm4x3tO9BpM%2F%2BXNhHZpXIil5%2FEkQu) | [http://www.testlocation.com:7001/](https://link.segmentfault.com/?enc=Jrdkq4aWOqmeiey4SgVvYg%3D%3D.435nkyhTerDZER51ASHlw9gl9W1J55elCDKfWaZn7QIBBLg9W%2BpttlTOU8ZJT53L) | 跨域         | 端口号不同（8080/7001）        |

## **为什么会有跨域问题**

### **源于浏览器的同源策略**

> 同源策略(Same Origin Policy) 是一种浏览器安全机制，用于限制来自不同源（域名、协议和端口）的网页之间的交互。它的目的是防止恶意网页通过脚本等方式访问和篡改其他网页的信息，保护用户的数据安全。同源策略要求网页只能与同源网页进行通信，即只能在相同的域名、协议和端口下进行交互，这样可以有效地防止跨站脚本攻击和数据泄露等安全问题的发生。

### **限制的范围**

- Cookie、LocalStorage、IndexdDB 等存储内容；
- DOM 节点；
- Ajax 请求

### **为什么Ajax请求不能跨域**

Ajax 其实就是向服务器发送一个 GET 或 POST 请求，然后取得服务器响应结果，返回客户端。

Ajax 跨域请求，在服务器端不会有任何问题，只是服务端响应数据返回给浏览器的时候，浏览器根据响应头的Access-Control-Allow-Origin字段的值来判断是否有权限获取数据。因此，服务端如果没有设置跨域字段设置，跨域是没有权限访问，数据被浏览器给拦截了。

## **如何解决跨域问题**

### **Nginx反向代理**

> Nginx反向代理服务器是一种常用的网络服务器软件，它可以将客户端的请求转发到后端服务器，并将响应返回给客户端。通过使用Nginx反向代理服务器，可以实现负载均衡、缓存加速、安全过滤等功能，提高网站的性能和安全性。

Nginx可以通过配置来解决跨域问题。下面是一种常见的解决方法：

在Nginx的配置文件中，可以添加以下代码来设置跨域请求头：



```
location / {
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
    add_header Access-Control-Expose-Headers 'Content-Length,Content-Range';
}
```



以上配置允许所有来源（*）的请求进行跨域访问，并设置了允许的请求方法和头部信息。

### **跨域资源共享CORS**

> CORS（Cross-Origin Resource Sharing）是一种浏览器安全机制，用于控制在Web应用程序中如何处理跨域资源请求。它允许一个域的网页向另一个域的服务器发送AJAX请求，并且可以限制哪些跨域请求是被允许的。通过在HTTP头中添加特定的CORS标头，服务器可以告诉浏览器哪些跨域请求是被允许的，从而解决了浏览器的同源策略限制。

**允许在下列场景中使用跨域 HTTP 请求：**

- 由 XMLHttpRequest 或 Fetch 发起的跨域 HTTP 请求
- Web 字体 (CSS 中通过 @font-face 使用跨域字体资源)
- WebGL 贴图
- 使用 drawImage 将 Images/video 画面绘制到 canvas

#### **简单请求、非简单请求**

浏览器将 CORS 请求分成两类：**简单请求**（simple request）和**非简单请求**（not-so-simple request）。

只要同时满足以下两大条件，就属于简单请求（不会触发 CORS 预检请求）。

- 请求方法是以下三种方法之一：HEAD、GET、POST
- HTTP 的头信息不超出以下几种字段：
- Accept
- Accept-Language
- Content-Language
- Last-Event-ID
- Content-Type（只限于三个值）
    - application/x-www-form-urlencoded
    - multipart/form-data
    - text/plain

凡是不同时满足上面两个条件，就属于非简单请求。

#### **CORS工作原理**

首先，浏览器判断请求是简单请求还是复杂请求（非简单请求）。

如果是复杂请求，那么在进行真正的请求之前，浏览器会先使用 OPTIONS 方法发送一个**预检请求** (preflight request)，OPTIONS 是 HTTP/1.1 协议中定义的方法，用以从服务器获取更多信息。

该方法不会对服务器资源产生影响，预检请求中同时携带了下面两个首部字段：

- Access-Control-Request-Method: 这个字段表明了请求的方法；
- Access-Control-Request-Headers: 这个字段表明了这个请求的 Headers；
- Origin: 这个字段表明了请求发出的域。

服务端收到请求后，会以 Access-Control-* response headers 的形式对客户端进行回复：

- Access-Control-Allow-Origin: 能够被允许发出这个请求的域名，也可以使用*来表明允许所有域名；
- Access-Control-Allow-Methods: 用逗号分隔的被允许的请求方法的列表；
- Access-Control-Allow-Headers: 用逗号分隔的被允许的请求头部字段的列表；
- Access-Control-Max-Age: 这个**预检请求能被缓存的最长时间**，在缓存时间内，同一个请求不会再次发出预检请求。

#### **简单请求**

对于简单请求，浏览器直接发出 CORS 请求。具体来说，就是在头信息之中，自动增加一个 Origin 字段，用来说明请求来自哪个源。服务器拿到请求之后，在回应时对应地添加Access-Control-Allow-Origin字段，如果 Origin 不在这个字段的范围中，那么浏览器就会将响应拦截。

# **相关文档**

https://segmentfault.com/a/1190000040485198