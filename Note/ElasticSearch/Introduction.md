# 什么是ElasticSearch

## **官方定义**

> Elasticsearch is the distributed search and analytics engine at the heart of the Elastic Stack. Logstash and Beats facilitate collecting, aggregating, and enriching your data and storing it in Elasticsearch. Kibana enables you to interactively explore, visualize, and share insights into your data and manage and monitor the stack. Elasticsearch is where the indexing, search, and analysis magic happens.

ElasticSearch是一款非常强大的、基于Lucene的开源搜索及分析引擎；它是一个实时的分布式搜索分析引擎，它能让你以前所未有的速度和规模，去探索你的数据。

## **基本概念**

- **节点 Node、集群 Cluster 和 分片 Shard**

ElasticSearch 是分布式数据库，允许多台服务器协同工作，每台服务器可以运行多个实例。单个实例称为一个节点（node），一组节点构成一个集群（cluster）。

分片是底层的工作单元，文档保存在分片内，分片又被分配到集群内的各个节点里，每个分片仅保存全部数据的一部分。分片是物理上的一个Lucene索引，包括主副本两种类型，同一个分片的主副本不会出现在一个实例上。

- **索引 Index、类型 Type 和 文档 Document**

索引是一类文档的结合。而文档是具体的一条数据。对比我们比较熟悉的关系型数据库如下：

| **RDBMS(MySQL)** | **Elasticsearch** |
| ---------------- | ----------------- |
| Table            | Index(Type) 索引  |
| Row              | Document 文档     |
| Column           | Field 字段        |
| Schema           | Mapping           |
| SQL              | DSL               |

## **ES整体架构**

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAAqQQ01j4um8O-K1dTi2X6_:-3936568112045838242fcAAqQQ01j4um8O-K1dTi2X6_:1707375246323)

### **快手ES业务交互架构图**

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAAqQQ01j4um8O-K1dTi2X6_:5722192112575945290fcAAqQQ01j4um8O-K1dTi2X6_:1707375246324)

## **Lucene 倒排索引**

原理图

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAAqQQ01j4um8O-K1dTi2X6_:-8775631940276914060fcAAqQQ01j4um8O-K1dTi2X6_:1707375246324)

Elasticsearch 的搜索原理简单过程是，索引系统通过扫描文章中的每一个词，对其创建索引，指明在文章中出现的次数和位置，当用户查询时，索引系统就会根据事先的索引进行查找，并将查找的结果反馈给用户的检索方式。

倒排索引包含两个部分：

- **单词词典（Term Dictionary）**：记录所有文档的单词，记录单词到倒排列表的关联关系（单词词典一般比较大，通过 B+ 树或哈希拉链法实现，以满足高性能的插入与查询）
- **倒排表（Posting List）**：记录了单词对应的文档结合，由倒排索引组成。
    - 文档ID
    - 词频 TF - 该单词在文档中分词的位置。用于语句搜索
    - 位置（Position）- 单词在文档中分词的位置，用于语句搜索
    - 偏移（Offset）- 记录单词的开始结束位置，实现高亮显示。

### **ES如何实现高可用**

Elasticsearch通过设置副本分片的方式来保证高可用，当集群中某个节点的主分片不可用时，集群将其他节点上的从副本分片提升为主分片

# **如何使用ES？**

使用流程图

![img](https://docs.corp.kuaishou.com/image/api/external/load/out?code=fcAAqQQ01j4um8O-K1dTi2X6_:1552025283914585974fcAAqQQ01j4um8O-K1dTi2X6_:1707375246324)

# **相关文档**

官网文档

https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html

https://zhuanlan.zhihu.com/p/451571598

https://kiosk007.top/post/elasticsearch/

https://pdai.tech/md/db/nosql-es/elasticsearch.html