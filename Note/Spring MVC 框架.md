# Spring MVC 框架

MVC模型，全称Model,View,Controller模型。Spring Web MVC 框架也是一个基于请求驱动的 Web 框架，并且也使用了前端控制器模式来进行设计，再根据请求映射规则分发给相应的页面控制器(动作/处理器)进行处理。首先让我们整体看一下 Spring Web MVC 处理请求的流程:

![image-20231008214513481](/Users/arpatnurmamat/Library/Application Support/typora-user-images/image-20231008214513481.png)







Model：数据模型

View：视图，负责模型的展示

Controller：控制器，接收用户的请求，委托给模型处理，处理完成后把结果返回给视图

模型：

![image-20231008214249068](/Users/arpatnurmamat/Library/Application Support/typora-user-images/image-20231008214249068.png)