# Part 3 Management Principles 

1. 一个组织成功与否和它如何处理反馈有重要关系

>  the most important difference between great organizations and bad ones is in how well they manage their feedback loops

2. 一个组织是由它的文化和它的人组成的，只要文化和人都好时，这个组织才能更好

3. 三个重要的问题

> what you want?  what is true?  what need to be done about it ?



## 相信真理 Trust in truth

1. Realize that you have nothing to fear from truth.

需要直面真相，不要害怕、担心面对真相会带来什么样的后果。

2. Create an environment in which everyone has the right to understand what makes sense and no one has the right to hold a critical opinion without speaking up about it.

3. Be extremely open

    不仅在工作中，在生活中也同样需要保持开放的心态。

4. **Have integrity and demand it from others**

    做一个正直的人，不要在别人背后说闲话。同时不要让“忠诚”挡住真相和开放。

5. **Be radically transparent**

​	不要藏事儿